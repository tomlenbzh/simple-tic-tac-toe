import { IPlayer } from '../interfaces/player.interface';

export enum Players {
  X,
  O
}

export const PlayersList: IPlayer[] = [
  {
    id: Players.X,
    label: 'X'
  },
  {
    id: Players.O,
    label: 'O'
  }
];
