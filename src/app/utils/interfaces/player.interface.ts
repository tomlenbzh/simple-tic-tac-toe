import { Players } from '../constants/players';

export interface IPlayer {
  id: Players;
  label: string;
}

export interface IBoardSquare {
  id: number;
  player: IPlayer | null;
  win?: boolean;
}
