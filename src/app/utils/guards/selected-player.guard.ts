import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { GameService } from 'src/app/services/game.service';

@Injectable({ providedIn: 'root' })
export class HasSelectedPlayerGuard implements CanActivate {
  constructor(private gameService: GameService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const hasSelectedPlayer = this.gameService.selectedPlayer ? true : false;
    !hasSelectedPlayer && this.router.navigateByUrl('');
    return hasSelectedPlayer;
  }
}
