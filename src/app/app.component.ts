import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'simple-tic-tac-toe';

  constructor(private titleService: Title) {
   this.titleService.setTitle(this.title); 
  }
}
