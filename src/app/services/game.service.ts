import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { PlayersList } from '../utils/constants/players';
import { IBoardSquare, IPlayer } from '../utils/interfaces/player.interface';

@Injectable({ providedIn: 'root' })
export class GameService {
  board: BehaviorSubject<IBoardSquare[]> = new BehaviorSubject<IBoardSquare[]>([]);
  boardSquares: IBoardSquare[] = [];
  boardSize: number = 9;
  turns: number = 0;

  isGameRunning: boolean = false;
  isGameOver: boolean = false;
  winner: boolean = false;

  selectedPlayer!: IPlayer;
  currentPlayer!: IPlayer;

  private playersList: IPlayer[] = PlayersList;

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  /**
   * Returns the game's board observable.
   */
  get _board(): BehaviorSubject<IBoardSquare[]> {
    return this.board;
  }

  /**
   * Returns whether a row, column or diagonal is entirely filled with one symbol.
   */
  get _isWinner(): boolean {
    return (
      this.matchingDiagonal() ||
      this.matchingRow(this.boardSquares, 'row') ||
      this.matchingRow(this.boardSquares, 'col')
    );
  }

  get isBoardFull(): boolean {
    return !this.boardSquares.some((x) => !x.player);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Sets board and game variables.
   */
  startNewGame(): void {
    this.currentPlayer = this.selectedPlayer;
    this.turns = 0;

    this.boardSquares = this.createBoard();
    this.board.next(this.boardSquares);

    this.isGameRunning = false;
    this.isGameOver = false;
    this.winner = false;
  }

  /**
   * Stores the selected player.
   *
   * @param     { IPlayer }      player
   */
  setSelectedPlayer(player: IPlayer): void {
    this.selectedPlayer = player;
  }

  /**
   * Updates the board with the selected square value and passes to the next turn.
   *
   * @param     { IBoardSquare }      square
   */
  nextTurn(square: IBoardSquare): void {
    this.updateBoard(square);

    const selectedIsCurrentPlayer = this.selectedPlayer.id === this.currentPlayer.id;
    this.currentPlayer = selectedIsCurrentPlayer
      ? this.playersList.find((player: IPlayer) => player.id !== this.currentPlayer.id)!
      : this.selectedPlayer;

    this.turns++;
    this.isGameOver = this.isGameOver;
  }

  /**
   * Sets the current square's content and assigns it to the board.
   *
   * @param     { IBoardSquare }      square
   */
  updateBoard(square: IBoardSquare): void {
    this.boardSquares[square.id].player = square.player;
    this.board.next(this.boardSquares);

    if (this._isWinner) {
      this.winner = true;
      this.isGameRunning = false;
      this.isGameOver = true;
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Checks if a diagonal is entirely filled with one symbol.
   *
   * @returns { boolean }
   */
  private matchingDiagonal(): boolean {
    const timesRun = 2;
    const midSquare: IBoardSquare = this.boardSquares[4];

    for (let i = 0; i <= timesRun; i += 2) {
      const upperCorner: IBoardSquare = this.boardSquares[i];
      const lowerCorner: IBoardSquare = this.boardSquares[8 - i];

      const cornersFilled: boolean = midSquare.player && upperCorner.player && lowerCorner.player ? true : false;
      const cornersMatchMiddle = midSquare.player === upperCorner.player && upperCorner.player === lowerCorner.player;

      if (cornersFilled && cornersMatchMiddle) {
        this.highlightWinningSquares([midSquare, upperCorner, lowerCorner]);
        return true;
      }
    }

    this.endsIfBoardIsFull();

    return false;
  }

  /**
   * Checks if a row or column is entirely filled with one symbol.
   *
   * @returns { boolean }
   */
  private matchingRow(board: IBoardSquare[], type: 'row' | 'col'): boolean {
    const distance = type === 'row' ? 1 : 3;
    const increment = type === 'row' ? 3 : 1;
    const numTimmes = type === 'row' ? 7 : 3;

    for (let i = 0; i < numTimmes; i += increment) {
      const firstSquare: IBoardSquare = board[i]!;
      const secondSquare: IBoardSquare = board[i + distance]!;
      const thirdSquare: IBoardSquare = board[i + distance * 2]!;

      const wholeRowFilled: boolean = firstSquare.player && secondSquare.player && thirdSquare.player ? true : false;
      const sidesMatchMiddle: boolean =
        firstSquare.player === secondSquare.player && secondSquare.player === thirdSquare.player;

      if (wholeRowFilled && sidesMatchMiddle) {
        this.highlightWinningSquares([firstSquare, secondSquare, thirdSquare]);
        return true;
      }
    }

    this.endsIfBoardIsFull();

    return false;
  }

  private highlightWinningSquares(winningSquares: IBoardSquare[]): void {
    this.boardSquares = this.boardSquares.map((square: IBoardSquare) => {
      const matchingSquare: IBoardSquare = winningSquares.find((wSquare) => square.id === wSquare.id)!;
      return matchingSquare ? { ...square, win: true } : square;
    });

    this.board.next(this.boardSquares);
  }

  /**
   * Ends game if the board has entirely been filled.
   */
  private endsIfBoardIsFull(): void {
    if (this.isBoardFull) {
      this.isGameOver = true;
      this.isGameRunning = false;
    }
  }

  /**
   * Fills the board with squares items.
   */
  private createBoard(): IBoardSquare[] {
    let board: IBoardSquare[] = [];

    for (let i = 0; i < this.boardSize; i++) {
      const newBoardSquare: IBoardSquare = { id: i, player: null };
      board.push(newBoardSquare);
    }

    return board;
  }
}
