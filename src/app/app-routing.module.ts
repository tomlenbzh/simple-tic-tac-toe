import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChooseYourSymbolComponent } from './pages/choose-your-symbol/choose-your-symbol.component';
import { PlayingAreaComponent } from './pages/playing-area/playing-area.component';
import { HasSelectedPlayerGuard } from './utils/guards/selected-player.guard';

const routes: Routes = [
  { path: 'choose-your-symbol', component: ChooseYourSymbolComponent },
  { path: 'playing-area', component: PlayingAreaComponent, canActivate: [HasSelectedPlayerGuard] },
  { path: '', pathMatch: 'full', redirectTo: 'choose-your-symbol' },
  { path: '**', redirectTo: 'choose-your-symbol' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
