import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { GameService } from 'src/app/services/game.service';
import { IBoardSquare } from 'src/app/utils/interfaces/player.interface';

@Component({
  selector: 'app-playing-area',
  templateUrl: './playing-area.component.html',
  styleUrls: ['./playing-area.component.scss']
})
export class PlayingAreaComponent implements OnInit {
  board!: Observable<IBoardSquare[]>;

  constructor(private gameService: GameService, private router: Router) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  isGameOver(): boolean {
    return this.gameService?.isGameOver;
  }

  isWinner(): boolean {
    return this.gameService?.winner && !this.isMyTurn();
  }

  isLoser(): boolean {
    return this.gameService?.winner && this.isMyTurn();
  }

  isDraw(): boolean {
    return !this.gameService?.winner && this.isGameOver();
  }

  isMyTurn(): boolean {
    return this.gameService?.currentPlayer?.id === this.gameService?.selectedPlayer?.id;
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    this.gameService.startNewGame();
    this.board = this.gameService._board;
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Assigns the selected square to the board and passes to the next turn.
   *
   * @param     { IBoardSquare }      square
   */
  selectSquare(square: IBoardSquare): void {
    if (this.gameService.winner) return;

    this.gameService.isGameRunning = true;

    if (this.gameService.isGameRunning && !square.player) {
      square.player = this.gameService.currentPlayer;
      this.gameService.nextTurn(square);
    }
  }

  resetGame(): void {
    this.router.navigateByUrl('');
  }
}
