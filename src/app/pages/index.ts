import { ChooseYourSymbolComponent } from './choose-your-symbol/choose-your-symbol.component';
import { PlayingAreaComponent } from './playing-area/playing-area.component';

export const pages = [ChooseYourSymbolComponent, PlayingAreaComponent];
