import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from 'src/app/services/game.service';
import { PlayersList } from 'src/app/utils/constants/players';
import { IPlayer } from 'src/app/utils/interfaces/player.interface';

@Component({
  selector: 'app-choose-your-symbol',
  templateUrl: './choose-your-symbol.component.html',
  styleUrls: ['./choose-your-symbol.component.scss']
})
export class ChooseYourSymbolComponent {
  playersList: IPlayer[] = PlayersList;

  constructor(private gameService: GameService, private router: Router) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Stores the selected player and navigates to the game board.
   *
   * @param     { IPlayer }      player
   */
  choosePlayer(player: IPlayer): void {
    this.gameService.setSelectedPlayer(player);
    this.router.navigateByUrl('playing-area');
  }

  /**
   * Stores a random player and naviagates to the game board.
   *
   * @param     { IPlayer }      player
   */
  chooseRandomPlayer(): void {
    const randomPlayer: IPlayer = this.playersList[Math.floor(Math.random() * this.playersList.length)];
    this.gameService.setSelectedPlayer(randomPlayer);
    this.router.navigateByUrl('playing-area');
  }

  /**
   * Tracks items in an iterable list thanks to their ids.
   *
   * @param     { number }      index
   * @param     { IPlayer }     player
   * @returns   { number }
   */
  trackByFn(index: number, player: IPlayer): number {
    return index || player.id;
  }
}
