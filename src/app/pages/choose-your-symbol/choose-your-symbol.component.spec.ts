import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChooseYourSymbolComponent } from './choose-your-symbol.component';

describe('ChooseYourSymbolComponent', () => {
  let component: ChooseYourSymbolComponent;
  let fixture: ComponentFixture<ChooseYourSymbolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChooseYourSymbolComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(ChooseYourSymbolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});
