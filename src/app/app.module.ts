import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { pages } from './pages';

@NgModule({
  declarations: [AppComponent, ...pages],
  imports: [BrowserModule, AppRoutingModule, NgbModule],
  bootstrap: [AppComponent]
})
export class AppModule {}
